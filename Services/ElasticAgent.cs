﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XMLParserModule.Models;

namespace XMLParserModule.Services
{
    public class ElasticAgent
    {
        private Globals globals;

        public async Task<ResultItem<GetParseXMLFieldModelResult>> GetParseXMLFieldModel(ParseXMLFieldRequest request)
        {
            var taskResult = await Task.Run<ResultItem<GetParseXMLFieldModelResult>>(() =>
            {
                var result = new ResultItem<GetParseXMLFieldModelResult>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new GetParseXMLFieldModelResult()
                };

                if (string.IsNullOrEmpty(request.IdConfig) || !globals.is_GUID(request.IdConfig))
                {
                    result.ResultState = globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "The Config-Id is not valid!";
                    return result;
                }

                var searchRootConfig = new List<clsOntologyItem>
                {
                    new clsOntologyItem
                    {
                        GUID = request.IdConfig,
                        GUID_Parent = Config.LocalData.Class_XMLParserModule.GUID
                    }
                };

                var dbReaderRootConfig = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderRootConfig.GetDataObjects(searchRootConfig);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Root-Config";
                    return result;
                }

                if (!dbReaderRootConfig.Objects1.Any())
                {
                    result.ResultState = globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "No Config found!";
                    return result;
                }

                var searchChildConfigs = dbReaderRootConfig.Objects1.Select(obj => new clsObjectRel
                {
                    ID_Object = obj.GUID,
                    ID_RelationType = Config.LocalData.ClassRel_XMLParserModule_contains_XMLParserModule.ID_RelationType,
                    ID_Parent_Other = Config.LocalData.ClassRel_XMLParserModule_contains_XMLParserModule.ID_Class_Right
                }).ToList();

                
                var dbReaderChildConfigs = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderChildConfigs.GetDataObjectRel(searchChildConfigs);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while reading child-configs";
                    return result;
                }

                result.Result.Configs = dbReaderChildConfigs.ObjectRels.Select(rel => new clsOntologyItem
                {
                    GUID = rel.ID_Other,
                    Name = rel.Name_Other,
                    GUID_Parent = rel.ID_Parent_Other,
                    Type = rel.Ontology
                }).ToList();

                if (!result.Result.Configs.Any())
                {
                    result.Result.Configs.AddRange(dbReaderRootConfig.Objects1);
                }

                var searchXMLElements = result.Result.Configs.Select(config => new clsObjectRel
                {
                    ID_Object = config.GUID,
                    ID_RelationType = Config.LocalData.ClassRel_XMLParserModule_search_Element__XML_.ID_RelationType,
                    ID_Parent_Other = Config.LocalData.ClassRel_XMLParserModule_search_Element__XML_.ID_Class_Right
                }).ToList();

                if (searchXMLElements.Any())
                {
                    var dbReaderXMLElements = new OntologyModDBConnector(globals);

                    result.ResultState = dbReaderXMLElements.GetDataObjectRel(searchXMLElements);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while reading XML-Elements";
                        return result;
                    }

                    result.Result.ConfigsToXMLElements = dbReaderXMLElements.ObjectRels;
                }

                var searchParseSourceFields = result.Result.Configs.Select(config => new clsObjectRel
                {
                    ID_Object = config.GUID,
                    ID_RelationType = Config.LocalData.ClassRel_XMLParserModule_ParseSource_Field.ID_RelationType,
                    ID_Parent_Other = Config.LocalData.ClassRel_XMLParserModule_ParseSource_Field.ID_Class_Right
                }).ToList();

                if (searchParseSourceFields.Any())
                {
                    var dbReaderParseSourceFields = new OntologyModDBConnector(globals);

                    result.ResultState = dbReaderParseSourceFields.GetDataObjectRel(searchParseSourceFields);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while reading Parse source fields";
                        return result;
                    }

                    result.Result.ConfigsToFieldsParseSource = dbReaderParseSourceFields.ObjectRels;
                }

                var searchDstFields = result.Result.Configs.Select(config => new clsObjectRel
                {
                    ID_Object = config.GUID,
                    ID_RelationType = Config.LocalData.ClassRel_XMLParserModule_dst_Field.ID_RelationType,
                    ID_Parent_Other = Config.LocalData.ClassRel_XMLParserModule_dst_Field.ID_Class_Right
                }).ToList();

                if (searchDstFields.Any())
                {
                    var dbReaderDstFields = new OntologyModDBConnector(globals);

                    result.ResultState = dbReaderDstFields.GetDataObjectRel(searchDstFields);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while reading Dest fields";
                        return result;
                    }

                    result.Result.ConfigsToFieldsDst = dbReaderDstFields.ObjectRels;
                }

                var searchFieldMaps = result.Result.Configs.Select(config => new clsObjectRel
                {
                    ID_Object = config.GUID,
                    ID_RelationType = Config.LocalData.ClassRel_XMLParserModule_contains_Field_Map.ID_RelationType,
                    ID_Parent_Other = Config.LocalData.ClassRel_XMLParserModule_contains_Field_Map.ID_Class_Right
                }).ToList();

                if (searchFieldMaps.Any())
                {
                    var dbReaderFieldMaps = new OntologyModDBConnector(globals);

                    result.ResultState = dbReaderFieldMaps.GetDataObjectRel(searchFieldMaps);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while reading Field-Maps";
                        return result;
                    }

                    result.Result.ConfigsToFieldMaps = dbReaderFieldMaps.ObjectRels;
                }

                var searchFieldMapToSrc = result.Result.ConfigsToFieldMaps.Select(rel => new clsObjectRel
                {
                    ID_Object = rel.ID_Other,
                    ID_RelationType = Config.LocalData.ClassRel_Field_Map_src_Field.ID_RelationType,
                    ID_Parent_Other = Config.LocalData.ClassRel_Field_Map_src_Field.ID_Class_Right
                }).ToList();

                if (searchFieldMapToSrc.Any())
                {
                    var dbReaderFieldMapToSrc = new OntologyModDBConnector(globals);

                    result.ResultState = dbReaderFieldMapToSrc.GetDataObjectRel(searchFieldMapToSrc);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while reading Field-Maps Source-Fields";
                        return result;
                    }

                    result.Result.FieldMapsToFieldsSrc = dbReaderFieldMapToSrc.ObjectRels;
                }

                var searchFieldMapToDst = result.Result.ConfigsToFieldMaps.Select(rel => new clsObjectRel
                {
                    ID_Object = rel.ID_Other,
                    ID_RelationType = Config.LocalData.ClassRel_Field_Map_dst_Field.ID_RelationType,
                    ID_Parent_Other = Config.LocalData.ClassRel_Field_Map_dst_Field.ID_Class_Right
                }).ToList();

                if (searchFieldMapToDst.Any())
                {
                    var dbReaderFieldMapToDst = new OntologyModDBConnector(globals);

                    result.ResultState = dbReaderFieldMapToDst.GetDataObjectRel(searchFieldMapToDst);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while reading Field-Maps Dest-Fields";
                        return result;
                    }

                    result.Result.FieldMapsToFieldsDst = dbReaderFieldMapToDst.ObjectRels;
                }

                var searchTextParserSrc = result.Result.Configs.Select(config => new clsObjectRel
                {
                    ID_Object = config.GUID,
                    ID_RelationType = Config.LocalData.ClassRel_XMLParserModule_src_Textparser.ID_RelationType,
                    ID_Parent_Other = Config.LocalData.ClassRel_XMLParserModule_src_Textparser.ID_Class_Right
                }).ToList();

                if (searchTextParserSrc.Any())
                {
                    var dbReaderTextParserSrc = new OntologyModDBConnector(globals);

                    result.ResultState = dbReaderTextParserSrc.GetDataObjectRel(searchTextParserSrc);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while reading Src Textparsers";
                        return result;
                    }

                    result.Result.ConfigsToTextParsersSrc = dbReaderTextParserSrc.ObjectRels;
                }

                var searchTextParserDst = result.Result.Configs.Select(config => new clsObjectRel
                {
                    ID_Object = config.GUID,
                    ID_RelationType = Config.LocalData.ClassRel_XMLParserModule_dst_Textparser.ID_RelationType,
                    ID_Parent_Other = Config.LocalData.ClassRel_XMLParserModule_dst_Textparser.ID_Class_Right
                }).ToList();

                if (searchTextParserDst.Any())
                {
                    var dbReaderTextParserDst = new OntologyModDBConnector(globals);

                    result.ResultState = dbReaderTextParserDst.GetDataObjectRel(searchTextParserDst);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while reading Dest Textparsers";
                        return result;
                    }

                    result.Result.ConfigsToTextParsersDst = dbReaderTextParserDst.ObjectRels;
                }

                return result;
            });
            return taskResult;
        }

        public ElasticAgent(Globals globals)
        {
            this.globals = globals;
        }
    }
}
