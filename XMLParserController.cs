﻿using ElasticSearchNestConnector;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using TextParserModule;
using XMLParserModule.Models;
using XMLParserModule.Services;

namespace XMLParserModule
{
    public class XMLParserController : AppController
    {
        public async Task<clsOntologyItem> ImportXMLFile(string xmlFilePath, string index, string type, string server, int port, List<string> parentContainer)
        {
            var taskResult = await Task.Run<clsOntologyItem>(() =>
            {
                var result = Globals.LState_Success.Clone();
                var xmlPath = new List<string>();

                var elementDocs = new List<clsAppDocuments>();
                var attributeDocs = new List<clsAppDocuments>();

                var dbReader = new clsUserAppDBSelector(server, port, index, 5000, Globals.Session);
                var dbWriter = new clsUserAppDBUpdater(dbReader);

                using(var xmlTextReader = new XmlTextReader(xmlFilePath))
                {
                    while( xmlTextReader.Read())
                    {
                        if (xmlTextReader.Name.ToLower() != "xml")
                        {
                            if (xmlTextReader.NodeType == XmlNodeType.Element)
                            {
                                var path = xmlPath.LastOrDefault() ?? string.Empty;

                                clsAppDocuments doc = null;
                                elementDocs.Add(new clsAppDocuments
                                {
                                    Id = Globals.NewGUID,
                                    Dict = new Dictionary<string, object>()
                                });

                                doc = elementDocs.Last();
                                doc.Dict.Add("Name", xmlTextReader.Name);
                                doc.Dict.Add("Path", string.Join(".", xmlPath));
                                if (!string.IsNullOrEmpty(xmlTextReader.Value))
                                {
                                    doc.Dict.Add("Value", xmlTextReader.Value);
                                }
                                
                                if (xmlTextReader.HasAttributes)
                                {
                                    xmlTextReader.MoveToFirstAttribute();
                                    var name = xmlTextReader.Name;
                                    var ix = 1;
                                    while (doc.Dict.ContainsKey(name))
                                    {
                                        name = $"{name}_{ix}";
                                        ix++;
                                    }
                                    doc.Dict.Add(name, xmlTextReader.Value);

                                    while (xmlTextReader.MoveToNextAttribute())
                                    {
                                        name = xmlTextReader.Name;
                                        ix = 1;
                                        while (doc.Dict.ContainsKey(name))
                                        {
                                            name = $"{name}_{ix}";
                                            ix++;
                                        }
                                        doc.Dict.Add(name, xmlTextReader.Value);
                                    }

                                }
                                if (!xmlTextReader.IsEmptyElement)
                                {
                                    xmlPath.Add(xmlTextReader.Name);
                                }
                                
                            }
                            else if (xmlTextReader.NodeType == XmlNodeType.EndElement)
                            {
                                if (xmlPath.Last() == xmlTextReader.Name)
                                {
                                    xmlPath.RemoveAt(xmlPath.Count - 1);
                                }
                                if (elementDocs.Count > 5000)
                                {
                                    result = dbWriter.SaveDoc(elementDocs, type);
                                    if (result.GUID == Globals.LState_Error.GUID)
                                    {
                                        return result;
                                    }
                                    elementDocs.Clear();
                                }
                            }
                            else if (xmlTextReader.NodeType == XmlNodeType.Text)
                            {
                                var doc = elementDocs.Last();
                                var name = "Value";
                                var ix = 1;
                                while (doc.Dict.ContainsKey(name))
                                {
                                    name = $"{name}_{ix}";
                                    ix++;
                                }
                                doc.Dict.Add(name, xmlTextReader.Value);
                            }

                            
                        }
                    }
                    if (elementDocs.Count > 0)
                    {
                        result = dbWriter.SaveDoc(elementDocs, type);
                        if (result.GUID == Globals.LState_Error.GUID)
                        {
                            return result;
                        }
                        elementDocs.Clear();
                    }
                }

                return result;
            });

            return taskResult;
        }
        public async Task<ResultItem<ParseXMLFieldResult>> ParseXMLField(ParseXMLFieldRequest request)
        {
            var taskResult = await Task.Run<ResultItem<ParseXMLFieldResult>>(async() =>
            {
                var result = new ResultItem<ParseXMLFieldResult>
                {
                    ResultState = Globals.LState_Success.Clone(),
                    Result = new ParseXMLFieldResult()
                };

                var elasticAgent = new ElasticAgent(Globals);

                var serviceResult = await elasticAgent.GetParseXMLFieldModel(request);

                result.ResultState = serviceResult.ResultState;

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    return result;
                }
                var textParserController = new TextParserController(Globals);

                foreach (var config in serviceResult.Result.Configs)
                {
                    var textParserSRC = serviceResult.Result.ConfigsToTextParsersSrc.Where(txtParser => txtParser.ID_Object == config.GUID).Select(txtP => new clsOntologyItem
                    {
                        GUID = txtP.ID_Other,
                        Name = txtP.Name_Other,
                        GUID_Parent = txtP.ID_Parent_Other,
                        Type = txtP.Ontology
                    }).FirstOrDefault();

                    if (textParserSRC == null)
                    {
                        result.ResultState = Globals.LState_Error.Clone();
                        result.ResultState.Additional1 = "No Source-Textparser provided";
                        return result;
                    }

                    var textParserDST = serviceResult.Result.ConfigsToTextParsersDst.Where(txtParser => txtParser.ID_Object == config.GUID).Select(txtP => new clsOntologyItem
                    {
                        GUID = txtP.ID_Other,
                        Name = txtP.Name_Other,
                        GUID_Parent = txtP.ID_Parent_Other,
                        Type = txtP.Ontology
                    }).FirstOrDefault();

                    if (textParserDST == null)
                    {
                        result.ResultState = Globals.LState_Error.Clone();
                        result.ResultState.Additional1 = "No Dest-Textparser provided";
                        return result;
                    }

                    var searchXMLElement = serviceResult.Result.ConfigsToXMLElements.Where(xml => xml.ID_Object == config.GUID).Select(rel => new clsOntologyItem
                    {
                        GUID = rel.ID_Other,
                        Name = rel.Name_Other,
                        GUID_Parent = rel.ID_Parent_Other,
                        Type = rel.Ontology
                    }).FirstOrDefault();

                    if (searchXMLElement == null)
                    {
                        result.ResultState = Globals.LState_Error.Clone();
                        result.ResultState.Additional1 = "No XML-Element to search for provided";
                        return result;
                    }

                    var parseSource = serviceResult.Result.ConfigsToFieldsParseSource.Where(parse => parse.ID_Object == config.GUID).Select(rel => new clsOntologyItem
                    {
                        GUID = rel.ID_Other,
                        Name = rel.Name_Other,
                        GUID_Parent = rel.ID_Parent_Other,
                        Type = rel.Ontology

                    }).FirstOrDefault();

                    if (parseSource == null)
                    {
                        result.ResultState = Globals.LState_Error.Clone();
                        result.ResultState.Additional1 = "No Parse Source provided";
                        return result;
                    }

                    var destField = serviceResult.Result.ConfigsToFieldsDst.Where(parse => parse.ID_Object == config.GUID).Select(rel => new clsOntologyItem
                    {
                        GUID = rel.ID_Other,
                        Name = rel.Name_Other,
                        GUID_Parent = rel.ID_Parent_Other,
                        Type = rel.Ontology

                    }).FirstOrDefault();

                    if (destField == null)
                    {
                        result.ResultState = Globals.LState_Error.Clone();
                        result.ResultState.Additional1 = "No Dest-Field provided";
                        return result;
                    }

                    var textParserSrcResult = await textParserController.GetTextParser(textParserSRC);

                    result.ResultState = textParserSrcResult.ResultState;

                    if (result.ResultState.GUID == Globals.LState_Error.GUID || !textParserSrcResult.Result.Any())
                    {
                        result.ResultState.Additional1 = "Error while getting the SRC Textparser";
                        return result;
                    }

                    var textParserDstResult = await textParserController.GetTextParser(textParserDST);

                    result.ResultState = textParserDstResult.ResultState;

                    if (result.ResultState.GUID == Globals.LState_Error.GUID || !textParserDstResult.Result.Any())
                    {
                        result.ResultState.Additional1 = "Error while getting the Dest Textparser";
                        return result;
                    }

                    var textParserFieldsSrcResult = await textParserController.GetParserFields(textParserSRC);

                    result.ResultState = textParserFieldsSrcResult.ResultState;

                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting the SRC Textparser-Fields";
                        return result;
                    }

                    var textParserFieldsDstResult = await textParserController.GetParserFields(textParserDST);

                    result.ResultState = textParserFieldsDstResult.ResultState;

                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting the Dest Textparser-Fields";
                        return result;
                    }

                    var mappings = (from fieldMap in serviceResult.Result.ConfigsToFieldMaps.Where(map => map.ID_Object == config.GUID)
                                   join srcField in serviceResult.Result.FieldMapsToFieldsSrc on fieldMap.ID_Other equals srcField.ID_Object
                                   join dstField in serviceResult.Result.FieldMapsToFieldsDst on fieldMap.ID_Other equals dstField.ID_Object
                                   select new { fieldMap, srcField, dstField });

                    var dbReader = new ElasticSearchNestConnector.clsUserAppDBSelector(textParserDstResult.Result.First().NameServer, textParserDstResult.Result.First().Port, textParserDstResult.Result.First().NameIndexElasticSearch, 5000, Globals.Session);
                    var dbWriter = new ElasticSearchNestConnector.clsUserAppDBUpdater(dbReader);

                    var docCount = 0;
                    var docResult = await textParserController.LoadData(textParserSrcResult.Result.First(), textParserFieldsSrcResult.Result, 1, 2000);
                    var docsToSave = new List<clsAppDocuments>();

                    docCount = docResult.Documents.Count;

                    var docs = docResult.Documents;
                    var scrollId = docResult.ScrollId;

                    
                    while (docCount < docResult.TotalCount)
                    {
                        docResult = await textParserController.LoadData(textParserSrcResult.Result.First(), textParserFieldsSrcResult.Result, docCount - 1, 2000, scrollId: scrollId);

                        if (!docResult.IsOK)
                        {
                            result.ResultState = Globals.LState_Error.Clone();
                            result.ResultState.Additional1 = "Error while getting the Documents";
                            return result;
                        }

                        docs.AddRange(docResult.Documents);
                        docCount = docs.Count;


                        
                    }

                    foreach (var doc in docs)
                    {
                        if (doc.Dict.ContainsKey(parseSource.Name) && doc.Dict[parseSource.Name] != null)
                        {
                            var content = doc.Dict[parseSource.Name].ToString();
                            try
                            {
                                var xmlDoc = new XmlDocument();
                                xmlDoc.LoadXml(content);
                                var elements = xmlDoc.GetElementsByTagName(searchXMLElement.Name);
                                foreach (XmlElement element in elements)
                                {
                                    var dict = new Dictionary<string, object>();
                                    var text = element.InnerText;

                                    foreach (var mapping in mappings)
                                    {
                                        dict.Add(mapping.dstField.Name_Other, doc.Dict[mapping.srcField.Name_Other]);
                                    }

                                    dict.Add(destField.Name, text);

                                    var userDoc = new clsAppDocuments { Id = Guid.NewGuid().ToString(), Dict = dict };
                                    docsToSave.Add(userDoc);
                                    if (docsToSave.Count > 5000)
                                    {
                                        result.ResultState = dbWriter.SaveDoc(docsToSave, textParserDstResult.Result.First().NameEsType);
                                        if (result.ResultState.GUID == Globals.LState_Error.GUID)
                                        {
                                            result.ResultState.Additional1 = "Error while saving Documents";
                                            return result;
                                        }
                                        docsToSave.Clear();
                                    }
                                }

                            }
                            catch (Exception ex)
                            {


                            }
                        }

                    }

                    if (docsToSave.Any())
                    {
                        result.ResultState = dbWriter.SaveDoc(docsToSave, textParserDstResult.Result.First().NameEsType);
                    }
                }

                return result;
            });

            return taskResult;

        }

        public XMLParserController(Globals globals) : base(globals)
        {
        }
    }
}
