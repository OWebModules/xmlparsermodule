﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XMLParserModule.Models
{
    public class ParseXMLFieldRequest
    {
        public string IdConfig { get; private set; }

        public ParseXMLFieldRequest(string idConfig)
        {
            IdConfig = idConfig;
        }
    }
}
