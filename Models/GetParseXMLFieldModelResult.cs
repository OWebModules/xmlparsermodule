﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XMLParserModule.Models
{
    public class GetParseXMLFieldModelResult
    {
        public clsOntologyItem RootConfig { get; set; }
        public List<clsOntologyItem> Configs { get; set; } = new List<clsOntologyItem>();

        public List<clsObjectRel> ConfigsToXMLElements { get; set; } = new List<clsObjectRel>();

        public List<clsObjectRel> ConfigsToFieldsParseSource { get; set; } = new List<clsObjectRel>();
        public List<clsObjectRel> ConfigsToFieldsDst { get; set; } = new List<clsObjectRel>();

        public List<clsObjectRel> ConfigsToFieldMaps { get; set; } = new List<clsObjectRel>();

        public List<clsObjectRel> FieldMapsToFieldsSrc { get; set; } = new List<clsObjectRel>();
        public List<clsObjectRel> FieldMapsToFieldsDst { get; set; } = new List<clsObjectRel>();

        public List<clsObjectRel> ConfigsToTextParsersSrc { get; set; } = new List<clsObjectRel>();
        public List<clsObjectRel> ConfigsToTextParsersDst { get; set; } = new List<clsObjectRel>();
    }
}
